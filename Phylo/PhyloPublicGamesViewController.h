//
//  PhyloPublicGamesViewController.h
//  Phylo
//
//  Created by Cody on 2013-06-02.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhyloGameInfoViewController.h"
#import "MapViewController.h"

@interface PhyloPublicGamesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate> {
    IBOutlet UITableView* searchesTable;
}
@property (retain)NSMutableArray *tableData;

@property (retain, nonatomic) IBOutlet UISearchBar *search;
@property (nonatomic, retain) UITableView* searchesTable;




@end
