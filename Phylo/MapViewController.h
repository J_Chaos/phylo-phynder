//
//  MapViewController.h
//  Phylo
//
//  Created by Kelly Kim on 6/20/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <coreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController<CLLocationManagerDelegate>
@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@end