//
//  PhyloScavengerHuntViewController.h
//  Phylo
//
//  Created by Cody on 2013-06-02.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhyloCreateViewController.h"

@interface PhyloScavengerHuntViewController : UIViewController {
    PhyloCreateViewController *createViewController;
}

-(IBAction)createGameClicked:(id)sender;

@end
