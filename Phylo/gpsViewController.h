//
//  gpsViewController.h
//  Phylo
//
//  Created by Kelly Kim on 6/18/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <coreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface gpsViewController : UIViewController
<CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    
    IBOutlet MKMapView *worldView;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UITextField *locationTitleField;
}

@end
