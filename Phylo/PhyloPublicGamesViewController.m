//
//  PhyloPublicGamesViewController.m
//  Phylo
//
//  Created by Cody on 2013-06-02.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloPublicGamesViewController.h"

@interface PhyloPublicGamesViewController ()

@end

@implementation PhyloPublicGamesViewController
@synthesize tableData;
@synthesize search;
@synthesize searchesTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Public Games", @"Public Games");
        self.tabBarItem.image = [UIImage imageNamed:@"second"];
    }
    return self;
}
							
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableData = [[NSMutableArray alloc]init];
    //UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:[self.search resignFirstResponder]];
//    [self.view addGestureRecognizer:tap];
    
    
    [self.search setDelegate:self];

	// Do any additional setup after loading the view, typically from a nib.
}
- (void) viewDidAppear:(BOOL)animated{
    NSLog(@"hello");
    //[self.search becomeFirstResponder];
    //NSLog(@"%c",);
    //[super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

//Customize the number of rows in Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

//Customize the appearance of table view cells
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    //set up the cell
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    
    if([indexPath row] == 0)
        cell.textLabel.text = [NSString stringWithFormat:@"MyLocation"];//
    else if ([indexPath row] == 1)
        cell.textLabel.text = [NSString stringWithFormat:@"Burnaby Campus Animal Hunt"];
    else if ([indexPath row] == 2)
        cell.textLabel.text = [NSString stringWithFormat:@"Surrey Campus Scavenger Hunt"];
    else if ([indexPath row] == 3)
        cell.textLabel.text = [NSString stringWithFormat:@"Fraser Valley Animal Frenzy"];
    else if ([indexPath row] == 4)
        cell.textLabel.text = [NSString stringWithFormat:@"Fun with Phylo @ SFU"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //open an alert with an OK and cancel button
    
    /*
    NSString *alertString = [NSString stringWithFormat:@"Congrats! You're now in the game! Good luck!"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];
    [alert show];
    [alert release];
    */
    
    /*
    PhyloGameInfoViewController* gameInfoViewController = [[PhyloGameInfoViewController alloc] initWithNibName:@"PhyloGameInfoViewController" bundle:nil];
    [self.navigationController pushViewController:gameInfoViewController animated:YES];
     */
    
    MapViewController *mapViewController = [[MapViewController alloc] initWithNibName:@"MapViewController" bundle:nil];
    [self.navigationController pushViewController:mapViewController animated:YES];//
}
//


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    NSLog(@"User searched for %@",searchBar.text);
    
    
}


- (void)dealloc {
    [search release];
    [super dealloc];
}
@end
