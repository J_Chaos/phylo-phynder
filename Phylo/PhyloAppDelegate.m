//
//  PhyloAppDelegate.m
//  Phylo
//
//  Created by Cody on 2013-06-02.
//  Copyright (c) 2013 Cody. All rights reserved.
//	

#import "PhyloAppDelegate.h"
	
#import "PhyloScavengerHuntViewController.h"

#import "PhyloPublicGamesViewController.h"

#import "PhyloSettingsViewController.h"

#import "PhyloMyStatsViewController.h"

#import "PhyloMyGamesViewController.h"

#import "PhyloCreateViewController.h"

#import "PhyloLoginViewController.h"

#import "PhyloTabBarViewController.h"
#import "gpsViewController.h"

@implementation PhyloAppDelegate

- (void)dealloc
{
    [_window release];
    [_tabBarController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    PhyloLoginViewController* logVC = [[PhyloLoginViewController alloc]initWithNibName:@"PhyloLoginViewController" bundle:nil];
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.

    self.tabBarController = [[[UITabBarController alloc] init] autorelease];
    
   
    
    
   // [self.window setRootViewController:loginController];
    
    
    MapViewController *gpscontroller = [[MapViewController alloc]initWithNibName:nil bundle:nil];
    
    UIViewController *viewController1 = [[[PhyloScavengerHuntViewController alloc] initWithNibName:@"PhyloScavengerHuntViewController" bundle:nil] autorelease]; //After creating a new UIViewController, you have to initialize it so that it can display its contents.  There are 6 screens here.  5 of them are displayed.  The self.tabBarController.viewControllers shows which screens are viewable.
    UIViewController *viewController2 = [[[PhyloPublicGamesViewController alloc] initWithNibName:@"PhyloPublicGamesViewController" bundle:nil] autorelease];
    UIViewController *viewController3 = [[[PhyloSettingsViewController alloc] initWithNibName:@"PhyloSettingsViewController" bundle:nil] autorelease];
    UIViewController *viewController4 = [[[PhyloMyStatsViewController alloc] initWithNibName:@"PhyloMyStatsViewController" bundle:nil] autorelease];
    UIViewController *viewController5 = [[[PhyloMyGamesViewController alloc] initWithNibName:@"PhyloMyGamesViewController" bundle:nil] autorelease];
    
    UINavigationController *navVC1 = [[UINavigationController alloc] initWithRootViewController:viewController1];
    UINavigationController *navVC2 = [[UINavigationController alloc] initWithRootViewController:viewController2];
    UINavigationController *navMyGames = [[UINavigationController alloc] initWithRootViewController:viewController5];
    
    NSArray *viewsArray = [NSArray arrayWithObjects:navVC1, navVC2, viewController3, viewController4, navMyGames, nil];
    
    self.tabBarController = [[[UITabBarController alloc] init] autorelease];
    //self.tabBarController.viewControllers = @[viewController1, viewController2, viewController3, viewController4, viewController5]; //The bottom tab bar can display 5 tabs at a time. If you want it to display, add the UIViewController to the end or replace the current ones with the one you want to display.  When there's more than 5, the last icon will be changed with "More.." and when you tap it, it will show a menu with links to the 5, 6, 7,... tabs.
    
    [self.tabBarController setViewControllers:viewsArray];
    //self.window.rootViewController = self.tabBarController;
    
    //[self.window addSubview:logVC.view];
    
   
    self.window.rootViewController = self.tabBarController;
    //self.window.rootViewController = gpscontroller;
    
    [self.window makeKeyAndVisible];
    [self.window.rootViewController presentViewController:logVC animated:NO completion:nil];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state. 
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
}
*/

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
}
*/

@end
