//
//  gpsViewController.m
//  Phylo
//
//  Created by Kelly Kim on 6/18/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "gpsViewController.h"

@interface gpsViewController ()

@end

@implementation gpsViewController

//CORE LOCATION METHOD
-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        //Create location manager object
        locationManager = [[CLLocationManager alloc]init];
        //accuracy = most accurate as possible
        [locationManager setDesiredAccuracy:(kCLLocationAccuracyBest)];
        
        //setting delegate
        [locationManager setDelegate:self];
        
  
    }
    return self;
}

//print device location
-(void)locationManager:(CLLocationManager*)manager
   didUpdateToLocation:(CLLocation *)newLocation
          fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"%@",newLocation);
}

//Location manager fails to find location
-(void)locationManager:(CLLocationManager*)manager
      didFailWithError:(NSError *)error
{
    NSLog(@"Could not find location: %@",error);
}

//Show user locaiton on map
-(void)viewDidLoad
{
    [worldView setShowsUserLocation: YES];
}

@end
