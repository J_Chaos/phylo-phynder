//
//  PhyloLoginViewController.m
//  Phylo
//
//  Created by Jacky Chao on 6/18/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloLoginViewController.h"
//#import "ASIHTTPRequest/ASIHTTPRequest.h"
#import "JSON/SBJson.h"
#import "ASIHTTPRequest/ASIFormDataRequest.h"
#import "UIDevice-with-UniqueIdentifier-for-iOS-5-master/UIDevice+IdentifierAddition.h"

@interface PhyloLoginViewController (){
    NSFileManager *fileManager;
    NSString *fullPath;
NSFileHandle *fileHandle; //

}

@end

@implementation PhyloLoginViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.tabVC = [[[UITabBarController alloc] init] autorelease];
    //setup path and filename to store and read from userinfo

    //Bool fileExist = [[NSFileManager]]
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_nameField release];
    [_passwordField release];
    [super dealloc];

}

#define username @"guest"
#define password @"abc123"
- (IBAction)Login:(UIButton *)sender {
    UIDevice *device = [UIDevice currentDevice];
    NSString *deviceID = [device uniqueDeviceIdentifier];
    
    NSString *inputUserName = self.nameField.text;
    NSString *inputPassword = self.passwordField.text;
    
    NSURL *url = [NSURL URLWithString:@"http://www.google.com/"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setPostValue:@"1" forKey:@"rw_app_id"];
    [request setPostValue:inputUserName forKey:@"username"];
    [request setPostValue:inputPassword forKey:@"password"];
    [request setPostValue:deviceID forKey:@"device_id"];
    [request setDelegate:self];
    [request startAsynchronous];
    
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Logged in." message:@"Congratuations! You are now logged in." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    if ([self.nameField.text isEqualToString:username] && [self.passwordField.text isEqualToString:password] ){
        [self dismissViewControllerAnimated:YES completion:nil];
        NSLog(@"Should return... %@ and %@.", self.nameField.text, self.passwordField.text);
    //[self.view addSubview:self.tabVC.view];
        [alert show];
        [alert release];
        
    }
    else{
        alert.title = @"Oops!";
        alert.message = @"Incorrect login information. Please try again.";
        [alert show];
        [alert release];
    }
    
}

- (IBAction)newUser:(UIButton *)sender {
}

- (void) authenticateHandler: (NSNotification*) notification name:(NSString*)inputname pass:(NSString*)inputpass{
    NSDictionary* userDict = [notification userInfo];
    BOOL loginStatus = [username isEqualToString:inputname] && [password isEqualToString:inputpass];
    
    [[NSUserDefaults standardUserDefaults] setBool:loginStatus forKey:@"userLoggedIn"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if( loginStatus ){
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Woops..." message:@"Username or password was not correct" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    
    

}

- (IBAction)backgroundClick:(id)sender{
    [self.nameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
    
}
- (void)requestFinished:(ASIHTTPRequest*)request{
    UIAlertView *alert = [[UIAlertView alloc]init];
    [alert addButtonWithTitle:@"OK"];
    if (request.responseStatusCode == 400){
        alert.title = @"Incorrect Account Information";
        alert.message = @"Are you sure you have entered the correct username/password? Please Try Again.";
        [alert buttonTitleAtIndex:0];
        [alert show];
    }
    else if (request.responseStatusCode == 200){
        NSString* responseString = [request responseString];
        NSDictionary* responseDict = [responseString JSONValue];
        NSString* unlockCode = [responseDict objectForKey:@"unlock_code"];
        
        if([unlockCode compare:@"com.razeware.test.unlock.cake"] == NSOrderedSame){
            alert.title = @"Congratulations!";
            alert.message = @"The cake is a lie!";
            [alert show];
        }
    
        }
    else
        NSLog(@"Unexpected Error...");//Error with Server
}


        

- (void)requestFailed:(ASIHTTPRequest*)request{
    NSError* error = [request error];
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    
}

@end
