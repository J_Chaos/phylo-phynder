//
//  WhereamIViewController.m
//  Phylo
//
//  Created by Kelly Kim on 6/18/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "WhereamIViewController.h"

@interface WhereamIViewController ()

@end

@implementation WhereamIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
