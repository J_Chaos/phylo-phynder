//
//  PhyloCreateViewController.m
//  Phylo
//
//  Created by Cody on 2013-06-04.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloCreateViewController.h"

@interface PhyloCreateViewController ()

@end

@implementation PhyloCreateViewController

@synthesize nameOfScavengerHunt;
@synthesize string = _string;

@synthesize privatePublic;
@synthesize password;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"Create a Game", @"Create a Game");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //////////////UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(backNavPressed)];
    //PhyloCreateViewController *_createViewController = [[PhyloCreateViewController alloc] initWithNibName:@"PhyloCreateViewController" bundle:nil];
    //[[self navigationController] pushViewController:_createViewController animated:YES];
    //UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    //////////////self.navigationItem.leftBarButtonItem = back;
    //[back release];
    //[_createViewController release], _createViewController = nil;
    /*
     UIImage *buttonImage = [UIImage imageNamed:@"first.png"];
     UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
     [button setImage:buttonImage forState:UIControlStateNormal];
     
     button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
     
     [button addTarget:self action:@selector(backNavPressed) forControlEvents:UIControlEventTouchUpInside];
     
     UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
     
     self.navigationItem.leftBarButtonItem = customBarItem;
     [customBarItem release];
     */
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction) privatePublicIndexChanged {
    switch (self.privatePublic.selectedSegmentIndex) {
        case 0:
            password.hidden = YES;
            break;
        case 1:
            password.hidden = NO;
            break;
        default:
            password.hidden = YES;
            break;
    }
}

-(IBAction) nextButtonPressed:(id)sender {
    PhyloSelectMapViewController* selectMapViewController = [[PhyloSelectMapViewController alloc] initWithNibName:@"PhyloSelectMapViewController" bundle:nil];
    [self.navigationController pushViewController:selectMapViewController animated:YES];
}

-(IBAction) dismissKeyboard:(id)sender{
    //[nameOfScavengerHunt resignFirstResponder];
    [sender resignFirstResponder];
    //return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == self.nameOfScavengerHunt)
    {
        [textField resignFirstResponder];
    }
    
    return YES;
}
@end
