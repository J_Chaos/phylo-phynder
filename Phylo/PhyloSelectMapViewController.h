//
//  PhyloSelectMapViewController.h
//  Phylo
//
//  Created by Cody Santos on 6/18/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <coreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface PhyloSelectMapViewController : UIViewController<CLLocationManagerDelegate>

@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@end